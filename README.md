# brandtii telemetry



## Information on the project

This GIT project contains code used to analyse M. brandtii telemetry data. Within this project we focused on estimating a time for each observation and using this value in different analyses.

The following analysis where performed for this project:
- homerange_analysis.R -> Performs a 95% and 50% homerange analysis using telemetry data.
The homeranges are written to shapefiles
- hotspot_analysis.R -> Performs a grid based hotspot analysis.
The sum of estimated obeservation time is derived per grid cell.
This value is used for a global-GI and local-GI (hotspot) analysis.
Global-GI is outputted in a txt file and the local-GI to a shape file containing the grid.
- Biotope_analysis.R -> Spatial information layers are used to derive biotope availiblity around roosts.
The same spatial information is used to derive time spend within these biotopes using the telemetry data.
The analysis outputs a png barplot highliting the difference between space availible and time spend.
Aditionally all values are written to excel files.

## Usage of scripts
All scripts can be used for their intended purpose independitly of the other scripts. 
This project does not contain any input data, so the scripts will have to be adapted to work for the desired input data.

## Contact information
Feel free to reach out if you have any questions regarding the code:

nino.menger@regelink.nl


