### Author: Nino Menger
### Date created: 23/10/2023
### Totorial used:
#   https://jamesepaterson.github.io/jamespatersonblog/04_trackingworkshop_kernels 
#   Please cite: 10.5281/zenodo.3557727
### Function: 
# Performs two 95% and 50% homerange analysis and saves to a shape file. 
# The first homerange analysis outputs results per individual.
# The second home range analysis outputs results per defined colony.


### Imports
library(sp)
library(sf)
library(adehabitatHR)



### Settings
## working directory
setwd("set_wd")

## Input
in_dist2roost <- "distance2roost.csv"

## Output
out_dir <- "HomeRangeShapes/"
out_perInd <- "ind"
out_perColony <- "colony"
homerange95 <- "_homerange95.shp"
homerange50 <- "_homerange50.shp"

## Colonies
Mbrandtii1 <- c(60, 62, 63)
Mbrandtii2 <- c(64, 66)



### Data loading and processing
dist_to_roost <- read.csv(in_dist2roost)


observations <- data.frame(
  id=dist_to_roost$bat,
  x=as.numeric(gsub("POINT \\(","",
         gsub(" [0-9].*","",dist_to_roost$observation_geom))),
  y=as.numeric(gsub(")","",
         gsub(".* ", "", dist_to_roost$observation_geom)))
)


observations.colony <- data.frame(observations)

temp.id <- observations.colony$id
observations.colony[temp.id %in% Mbrandtii1,]$id <- "Myotis brandtii 1"
observations.colony[temp.id %in% Mbrandtii2,]$id <- "Myotis brandtii 2"





### Homerange analysis
coordinates(observations) <- c("x", "y")
proj4string(observations) <- CRS( "+init=EPSG:28992" )
kernel.ref <- kernelUD(observations, h = "href")
#image(kernel.ref)

kernel.poly <- getverticeshr(kernel.ref, percent=95)
sf::st_write(as(kernel.poly, "sf"), paste0(out_dir, "/", out_perInd, homerange95))

kernel.poly <- getverticeshr(kernel.ref, percent=50)
sf::st_write(as(kernel.poly, "sf"), paste0(out_dir, "/", out_perInd, homerange50))



coordinates(observations.colony) <- c("x", "y")
proj4string(observations.colony) <- CRS( "+init=EPSG:28992" )
kernel.ref <- kernelUD(observations.colony, h = "href")
#image(kernel.ref)

kernel.poly <- getverticeshr(kernel.ref, percent=95)
sf::st_write(as(kernel.poly, "sf"), paste0(out_dir, "/", out_perColony, homerange95))

kernel.poly <- getverticeshr(kernel.ref, percent=50)
sf::st_write(as(kernel.poly, "sf"), paste0(out_dir, "/", out_perColony, homerange50))


